# -*- coding: utf-8 -*-

from requests import Session


__all__ = [
    "NextcloudSession",
]


class NextcloudSession(object):
    """Deal with nextcloud session in a generalised way."""

    def __init__(self, url, auth=None):
        """Create a session to a NextCloud instance.

        :param url: URL of the server.
        :type url: str

        :param auth: Authentification login/password.
        """
        self._url = url
        self._session = Session()
        if auth:
            self._session.auth = auth

    @property
    def session(self):
        """Return the session object.

        Allow to customize headers and other options to send with a request.
        """
        return self._session

    def _format(self, key, value):
        if key == "tags" or key == "search":
            lst = list()
            for v in value:
                lst.append(f"{key}[]={v}")

            format = "&".join(lst)
        else:
            format = f"{key}={value}"

        return format

    def get(self, route=None, **kwargs):
        """GET request."""
        path = self._url

        if route is not None and route != "":
            path = "/".join([self._url, route])

        return self._session.get(path, **kwargs)

    def post(self, route, **kwargs):
        """POST request."""
        return self._session.post("/".join([self._url, route]), **kwargs)

    def put(self, route, **kwargs):
        """PUT request."""
        return self._session.put("/".join([self._url, route]), **kwargs)

    def delete(self, route, **kwargs):
        """DELETE request."""
        return self._session.delete("/".join([self._url, route]), **kwargs)

    def __repr__(self):
        """Represent the object."""
        return f"< NextCloud seesion: '{self._url}' >"
