# -*- coding: utf-8 -*-

"""Some utilitary function not linked to anything, or used at multiple place.

:author: Guillaume Plum
"""


from base64 import decodebytes, encodebytes
from json import loads, dumps
from sjcl import SJCL


__all__ = [
    "to_json",
    "from_json",
    "decrypt",
    "encrypt",
]


def to_json(string):
    """Decode vaults string to JSON.

    :param string: String to decode.
    :type string: str or bytes

    :return: The JSON data.
    :rettype: dict
    """
    if isinstance(string, str):
        string = string.encode()

    b64 = decodebytes(string)

    return loads(b64.decode())


def from_json(data):
    """Encode JSON to vaults string.

    :param data: JSON data to encode.
    :type data: dict

    :return: The encoded string.
    :rettype: str
    """
    data_dump = dumps(data)
    encoded = encodebytes(data_dump.encode())

    return encoded.decode()


def decrypt(cipher, key):
    """Decrypt the given cipher using the key.

    :param cipher: The cipher structure to decrypt.
    :type cipher: dict
    :param key: Key to use to decrypt the cipher.
    :type key: str

    :return: The decrypted string.
    :rettype: str
    """
    inst = SJCL()

    decrypted = inst.decrypt(cipher, key)

    return decrypted.decode()


def encrypt(text, key):
    """Encrypt the given text using the given key.

    :param text: String to encrypt.
    :type text: str or bytes
    :param key: Key to encrypt with.
    :type key: str

    :return: Structure containing the encrypted text and relevant information.
    :rettype: dict
    """
    if isinstance(text, str):
        text = text.encode()

    return SJCL().encrypt(text, key, dkLen=32)


def from_vault(string, key):
    """Perform decryption for vault string.

    :param string: String taken from the vault.
    :type string: str
    :param key: Key use for encryption.
    :type key: str

    :return: Decrypted string.
    :rettype: str
    """
    cipher = to_json(string)

    return decrypt(cipher, key)


def to_vault(string, key):
    """Perform encryption for vault string.

    >>> to_vault("Beau temps", "Toto98")

    :param string: String to encrypt for the vault.
    :type string: str
    :param key: Key use for encryption.
    :type key: str

    :return: Decrypted string.
    :rettype: str
    """
    cipher = encrypt(string, key)
    to_base = from_json(cipher)

    return to_base.decode()
