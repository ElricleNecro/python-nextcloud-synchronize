# -*- coding: utf-8 -*-

"""Bookmark getter.

:author: Guillaume Plum.
"""


from .session import NextcloudSession


__all__ = [
    "Bookmarks",
    "Tags",
]

TAGS = "/apps/bookmarks/public/rest/v2/tag"


class Bookmarks(NextcloudSession):
    """Bookmarks class for nextcloud."""

    BOOKMARKS = "/apps/bookmarks/public/rest/v2"
    _route = "bookmark"

    def __init__(self, url, auth=None):
        """Create a bookmark session instance.

        :param url: URL of the server.
        :type url: str

        :param auth: Authentification login/password.
        """
        super(Bookmarks, self).__init__(
            "/".join([url, Bookmarks.BOOKMARKS]),
            auth=auth
        )

    def _format(self, key, value):
        if key == "tags" or key == "search":
            lst = list()
            for v in value:
                lst.append(f"{key}[]={v}")

            format = "&".join(lst)
        else:
            format = f"{key}={value}"

        return format

    def query(self, **kwargs):
        """Query bookmarks.

        :return: JSON formatted answer.
        """
        options = dict(
            tags=None,
            # Array of tags that returned bookmark should have.
            conjunction="or",
            # Set to and if require all tags to be present.
            page=-1,
            # If set to non-negative number, results will be paginated by 10 bookmark a page.
            sortby="lastmodified",
            # How to sort: 'url', 'title', 'description', 'public', 'lastmodified', 'clickcount'.
            search=None,
            # Array of word to search in url, title and description.
            user=None,
            # Return public bookmark of the user passed as this parameter.
        )
        options.update(kwargs)

        req_opt = "&".join(
            [self._format(key, value) for key, value in options.items() if value is not None]
        )

        print(req_opt)

        resp = self.get(f"{self._route}?{req_opt}")
        return resp.json()

    def create(self, url, **kwargs):
        """Create a bookmark.

        Example of the request:
            POST /apps/bookmarks/public/rest/v2/bookmark?url=http%3A%2F%2Fgoogle.com&title=Google&description=in%20case%20you%20forget&item[tags][]=search-engines&item[tags][]=uselessbookmark

        :param url: URL of the new bookmark.
        :type url: str
        :return: JSON formatted answer.
        """
        options = dict(
            url=url,
            # URL of the new bookmark.
            item=None,
            # Array of tags for this bookmark (item[tags][]=mon_tag)
            title=url,
            # The title of the bookmark.
            is_public=False,
            # Set this parameter without a value to mark as public.
            description=url,
            # Description of this bookmark.
        )
        options.update(kwargs)

        req_opt = "&".join(
            [self._format(key, value) for key, value in options.items() if value is not None]
        )

        print(req_opt)

        resp = self.get(f"{self._route}?{req_opt}")
        return resp.json()

    def edit(self, id, url, **kwargs):
        """Edit a bookmark.

        Example:
            PUT /apps/bookmarks/public/rest/v2/bookmark/:7

        :param id: Id of the bookmark.
        :param url: New URL.
        :type url: str

        :return: JSON formatted answer.
        """
        options = dict(
            id=id,
            # The id of the bookmark to edit.
            record_id=id,
            # The id of the bookmark to edit
            url=url,
            # New URL of the bookmark.
            item=None,
            # Array of tags for this bookmark (item[tags][]=mon_tag)
            title=url,
            # The title of the bookmark.
            is_public=False,
            # Set this parameter without a value to mark as public.
            description=url,
            # Description of this bookmark.
        )
        options.update(kwargs)

        req_opt = "&".join(
            [self._format(key, value) for key, value in options.items() if value is not None]
        )

        print(req_opt)

        resp = self.put(f"{self._route}?{req_opt}")
        return resp.json()

    def delete(self, id):
        """Delete a bookmark.

        Example:
            DELETE /apps/bookmarks/public/rest/v2/bookmark/7

        :param id: Id of the bookmark.
        :return: JSON formatted answer.
        """
        options = dict(
            id=id,
            # The id of the bookmark to edit.
        )

        print(f"{self._route}/{options['id']}")
        return None

        resp = self.delete(f"{self._route}/{options['id']}")
        return resp.json()


class Tags(NextcloudSession):
    """Tags associated with bookmarks."""

    TAGS = "/apps/bookmarks/public/rest/v2/tag"

    def __init__(self, url, auth=None):
        """Create a tags session instance.

        :param url: URL of the server.
        :type url: str

        :param auth: Authentification login/password.
        """
        super(Tags, self).__init__(
            "/".join([url, Tags.TAGS]),
            auth=auth
        )

    def get_tags(self):
        """Return all tags."""
        resp = self.get("")
        return resp.json()

    def delete_tag(self, tag):
        """Delete a tag.

        :param tag: tag to remove.
        :type tag: str
        """
        resp = self.get("?old_name=%s" % tag)
        return resp.json()

    def rename_tag(self, old, new):
        """Rename old tag to new tag.

        :param old: old name.
        :type old: str
        :param new: new name.
        :type new: str
        """
        resp = self.get("?old_name={}&new_name={}".format(old, new))
        return resp.json()
