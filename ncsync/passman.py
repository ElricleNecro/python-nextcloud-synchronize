# -*- coding: utf-8 -*-

"""Bookmark getter.

:author: Guillaume Plum.
"""


from .session import NextcloudSession


__all__ = [
    "Vaults",
]


class Vault(NextcloudSession):
    """Deal with a specific vault."""

    def __init__(self, guid, url, auth=None):
        """Create a Vaults session instance.

        :param url: URL of the server.
        :type url: str

        :param auth: Authentification login/password.
        """
        super(Vaults, self).__init__(
            "/".join([url, Vaults.VAULTS]),
            auth=auth
        )
        if isinstance(guid, str):
            self._vault = self.get(f"{guid}")
        elif isinstance(guid, dict):
            self._vault = guid


class Vaults(NextcloudSession):
    """Bookmarks class for nextcloud."""

    VAULTS = "/apps/passman/api/v2/vaults"
    _default_cipher_data = {
        "v": 1,
        "iter": 10000,
        "ks": 128,
        "ts": 64,
        "mode": "ccm",
        "adata": "",
        "cipher": "aes",
    }

    def __init__(self, url, auth=None):
        """Create a Vaults session instance.

        :param url: URL of the server.
        :type url: str

        :param auth: Authentification login/password.
        """
        super(Vaults, self).__init__(
            "/".join([url, Vaults.VAULTS]),
            auth=auth
        )

        self._vaults_list = None

    def list(self):
        """Query vaults.

        :return: JSON formatted answer.
        """
        self._vaults_list = self.get().json()
        return self._vaults_list

    def vault(self, guid):
        """Get specified vault.

        :param guid: guid of the vault.
        :type guid: int or str

        :return: JSON formatted answer.
        """
        return self.get(f"{guid}").json()

    def __str__(self):
        """Convert the object into string."""
        concat = ", ".join(
            [v["name"] for v in self.list()]
        )

        return "<[ %s ]>" % concat
