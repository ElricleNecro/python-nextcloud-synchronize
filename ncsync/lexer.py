#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pygments.lexer import RegexLexer, words
from pygments import token as tk


__all__ = [
    "NewsLexer",
]


class NewsLexer(RegexLexer):
    """Lexer for the News DSL.
    """

    tokens = {
        'root': [
            #  (r'"', tk.String, 'string'),
            #  (r"'", tk.String, 'string2'),
            (r'\n', tk.Text),
            (r'[^\S\n]+', tk.Text),
            # StringLiteral
            # -- raw_string_lit
            (r"'[^']*'", tk.String),
            # -- interpreted_string_lit
            (r'"(\\\\|\\"|[^"])*"', tk.String),
            (r'#(.*?)\n', tk.Comment.Single),
            (r'[0-9]+', tk.Number),
            (words(("get", "put", "starred")), tk.Name.Builtin),
            #  (r"(get|put|starred)", tk.Keyword),
            (r".", tk.Text),
        ],
    }
