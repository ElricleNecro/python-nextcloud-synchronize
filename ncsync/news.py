#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .session import NextcloudSession


"""
Actual API implemented:
https://github.com/nextcloud/news/blob/master/docs/externalapi/Legacy.md
For the more recent API:
NEWS="/apps/news/api/v2/"
https://github.com/nextcloud/news/blob/master/docs/externalapi/External-Api.md
resp = r.get(BASE_URL + NEWS, auth=('USER', 'PASSWD'), headers={"Accept": "application/json"})
"""


__all__ = [
    "News",
]


class News(NextcloudSession):
    """Class around the nextcloud news REST API.
    """

    # https://github.com/nextcloud/news/blob/master/docs/externalapi/Legacy.md
    NEWS = "/apps/news/api/v1-2"

    def __init__(self, url, auth=None):
        super(News, self).__init__("/".join([url, News.NEWS]), auth=auth)

    # Folders:
    def get_folders(self):
        """Return a list of the folders.
        """
        resp = self.get("/folders")
        return resp.json()

    # Feeds:
    def get_feeds(self):
        """Return a list of the feeds.
        """
        resp = self.get("/feeds")
        return resp.json()

    # Unread
    def get_unread(self):
        """Return unread feeds.
        """
        resp = self.get("/items?type=3&getRead=false&batchSize=-1")
        return resp.json()

    # Starred
    def get_starred(self):
        """Return starred feeds.
        """
        resp = self.get("/items?type=2&getRead=true&batchSize=-1")
        return resp.json()
