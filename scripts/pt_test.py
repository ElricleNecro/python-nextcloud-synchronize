#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse as ag
import logging as log

from prompt_toolkit import prompt
from prompt_toolkit.formatted_text import HTML
from prompt_toolkit.history import FileHistory
from prompt_toolkit.layout.lexers import Lexer
from prompt_toolkit.styles.named_colors import NAMED_COLORS
from prompt_toolkit.contrib.completers import WordCompleter
from prompt_toolkit.layout.lexers import PygmentsLexer
from prompt_toolkit.styles import style_from_pygments

from pygments.styles import get_style_by_name

from ncsync import lexer as lex


module_logger = log.getLogger("pt_test")
_level = [
    log.ERROR,
    log.WARN,
    log.INFO,
    log.DEBUG,
]
keyword = [
    'alligator',
    'ant',
    'ape',
    'bat',
    'bear',
    'beaver',
    'bee',
    'bison',
    'butterfly',
    'cat',
    'chicken',
    'crocodile',
    'dinosaur',
    'dog',
    'dolphine',
    'dove',
    'duck',
    'eagle',
    'elephant',
    'fish',
    'goat',
    'gorilla',
    'kangaroo',
    'leopard',
    'lion',
    'mouse',
    'rabbit',
    'rat',
    'snake',
    'spider',
    'turkey',
    'turtle',
]


class RainbowLexer(Lexer):
    _logger = log.getLogger("pt_test.RainbowLexer")

    def lex_document(self, document):
        colors = list(sorted(NAMED_COLORS, key=NAMED_COLORS.get))

        def get_line(lineno):
            RainbowLexer._logger.info(
                "Getting line '%s'." % document.lines[lineno]
            )

            ret = [
                (colors[i % len(colors)], c)
                for i, c in enumerate(document.lines[lineno])
            ]

            RainbowLexer._logger.debug(
                f"Returning {ret}"
            )

            return ret

        return get_line


if __name__ == "__main__":
    parser = ag.ArgumentParser(
        formatter_class=ag.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "-v", "--verbosity",
        action="count",
        default=0,
        help="Verbosity of the program."
    )

    parser.add_argument(
        "--log-file",
        type=str,
        default="/tmp/pt_test.log",
        help="Verbosity of the program."
    )

    args = parser.parse_args()

    if args.verbosity >= len(_level):
        args.verbosity = -1

    module_logger.setLevel(args.verbosity)

    fh = log.FileHandler(args.log_file)
    fh.setLevel(_level[args.verbosity])
    fh.setFormatter(
        log.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
    )
    module_logger.addHandler(fh)

    our_history = FileHistory('/tmp/example-history-file')

    animal_completer = WordCompleter(keyword, ignore_case=True)

    text = prompt(
        '>>> ',
        history=our_history,
        bottom_toolbar=HTML(
            '(html) <b>This</b> <u>is</u> a '
            '<style bg="ansired">toolbar</style>'
        ),
        #  lexer=RainbowLexer(),
        lexer=PygmentsLexer(lex.NewsLexer),
        style=style_from_pygments(get_style_by_name("vim")),
    )
