#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""Main program.
"""


import argparse as ag

from ncsync import News


BASE_URL = "https://ncloud.zaclys.com/index.php"


def parse_args():
    parser = ag.ArgumentParser(
        formatter_class=ag.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "-v", "--verbosity",
        action="count",
        default=0,
        help="Verbosity of the program."
    )

    sub_parsers = parser.add_subparsers(
        title="Nextcloud apps",
        description="Supported nextcloud applications.",
        help="Choose your app!",
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    n = News(BASE_URL)
    print(n.get_folders())
